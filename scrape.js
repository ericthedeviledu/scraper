const puppeteer = require('puppeteer');

let searchTerm;
// the selector for the agree consent button.
let consentButtonSelector = '#guce_index > div.l-container > div > div > div > div.final-btn-consent > div > button.btn.yes';
// the sections we scrape.
let scrapeSection = 'section.post_content';

async function Scraping() {
    try {
        const browser = await puppeteer.launch({
            headless: true,
            ignoreDefaultArgs: ["--hide-scrollbars"]
        });
        // note : required to use setSearchValue first prior to this function.
        let searchTerm = GetSearchValue();
        // opens the browser
        const page = await browser.newPage();
        // go to the new page 
        if (searchTerm != null) {
            await page.goto('https://www.tumblr.com/search/' + searchTerm + '/recent');
        } else {
            await page.goto('https://www.tumblr.com/explore/trending');
        }
        // evaluate the page and add everything in the post_content into a map
        const textContent = await page.evaluate(
            () =>
            Array.from(document.querySelectorAll('section.post_content'))
            .map(compact => compact.innerText.trim())
        );

        //console.log(textContent);

        // close the browser
        await browser.close();
        return textContent;

    } catch (error) {
        await page.waitForSelector(consentButtonSelector, {
            timeout: 5000
        })
        await page.click(consentButtonSelector);
        console.error(error);
        process.exit(1);
    }
}
// setter for the search term
function SetSearchValue(words) {
    searchTerm = words;
}
// getter for the search term;
function GetSearchValue() {
    return searchTerm;
}

/*  
    example of calling 
    SetSearchValue('potato');
    Scraping();
*/